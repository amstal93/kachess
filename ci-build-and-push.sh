#!/usr/bin/env sh

if [ ! -z ${CI_COMMIT_TAG} ]; then
docker buildx build \
	. \
	--platform linux/amd64,linux/arm64 \
	--push \
	-t ${CI_REGISTRY_IMAGE}:git-${CI_COMMIT_SHORT_SHA} \
	-t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} \
	-t ${CI_REGISTRY_IMAGE}:$(echo ${CI_COMMIT_TAG} | grep -qE '^v[0-9]+' && echo 'latest' || echo 'latest-dev')
else
	docker buildx build \
		. \
		--platform linux/amd64,linux/arm64 \
		--push \
		-t ${CI_REGISTRY_IMAGE}:git-${CI_COMMIT_SHORT_SHA}
fi
